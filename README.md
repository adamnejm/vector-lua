
# **Vector.lua** - Simple vector library for Lua

## Usage

Methods in the **past tense** produce a new Vector object, while **present tense** self-modifies the Vector object and returns it.  
Exception to this are functions that do not return a Vector such as `distance` or `length`, but also `clone` which duplicates a Vector.  
All meta-event operators such as `*` *(multiplication)* or `+` *(addition)* produce a new Vector object.

## Example

```lua
local Vector = require "vector"

local position = Vector(1, -3, 5)
local boxed = position:clamped(Vector(0), Vector(3))
print(boxed) --> [1, 0, 3]

local random_unit = Vector.random():mul(5)
print(random_unit:rounded()) --> [3, -2, 4]
```

## Documentation
Every function is documented using [sumneko's LSP](https://github.com/LuaLS/lua-language-server) annotation system.  
Meaning you can get dynamic syntax checking and autocompletion in [any code editor](https://microsoft.github.io/language-server-protocol/implementors/tools/) that supports the Language Server Protocol.
