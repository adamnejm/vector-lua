
---@diagnostic disable: duplicate-set-field, duplicate-doc-field

---@class Vector
---@field x number X component
---@field y number Y component
---@field z number Z component
---@operator mul(Vector|number):Vector
---@operator div(Vector|number):Vector
---@operator add(Vector):Vector
---@operator sub(Vector):Vector
---@operator unm:Vector
local Vector = {}


-------------------------------------------
--                HELPERS                --
-------------------------------------------

local function round(value, decimals)
	local mul = decimals and (10 ^ decimals) or 1
	return math.floor(value * mul + 0.5) / mul
end

local function clamp(value, min, max)
	return math.min(math.max(value, min), max)
end


-------------------------------------------
--             CONSTRUCTORS              --
-------------------------------------------

--- Creates a new Vector object from X, Y, Z components
---@param x? number X component
---@param y? number Y component
---@param z? number Z component
---@return Vector vector
local function new(x, y, z)
	if not x then x = 0 end
	if not z then z = y and 0 or x end
	if not y then y = x end
	return setmetatable({ x = x, y = y, z = z }, Vector)
end

--- Creates a new Vector object from a table
---@param tbl table Table containing X, Y, Z keys
---@return Vector vector
local function wrap(tbl)
	return setmetatable(tbl, Vector)
end

--- Returns a random unit Vector
---@return Vector vector
local function random()
	return new(-0.5 + math.random(), -0.5 + math.random(), -0.5 + math.random()):normalize()
end


-------------------------------------------
--               METATABLE               --
-------------------------------------------

---@private
Vector.__index = Vector

---@private
function Vector.__mul(a, b)
	if type(b) == "number" then
		return wrap { x = a.x * b,   y = a.y * b,   z = a.z * b }
	elseif type(a) == "number" then
		return wrap { x = b.x * a,   y = b.y * a,   z = b.z * a }
	else
		return wrap { x = a.x * b.x, y = a.y * b.y, z = a.z * b.z }
	end
end

---@private
function Vector.__div(a, b)
	if type(b) == "number" then
		return wrap { x = a.x / b,   y = a.y / b,   z = a.z / b }
	elseif type(a) == "number" then
		return wrap { x = b.x / a,   y = b.y / a,   z = b.z / a }
	else
		return wrap { x = a.x / b.x, y = a.y / b.y, z = a.z / b.z }
	end
end

---@private
function Vector.__add(a, b)
	return wrap { x = a.x + b.x, y = a.y + b.y, z = a.z + b.z }
end

---@private
function Vector.__sub(a, b)
	return wrap { x = a.x - b.x, y = a.y - b.y, z = a.z - b.z }
end

---@private
function Vector.__eq(a, b)
	return getmetatable(b) == Vector and a.x == b.x and a.y == b.y and a.z == b.z
end

---@private
function Vector:__unm()
	return wrap { x = -self.x, y = -self.y, z = -self.z }
end

---@private
function Vector:__tostring()
	return string.format("[%s, %s, %s]", self.x, self.y, self.z)
end


-------------------------------------------
--                METHODS                --
-------------------------------------------

--- Returns a new Vector with the same X, Y, Z components
---@return Vector clone
function Vector:clone()
	return wrap { x = self.x, y = self.y, z = self.z }
end

--- Returns all components of the Vector
---@return number x X component
---@return number y Y component
---@return number z Z component
function Vector:unpack()
	return self.x, self.y, self.z
end

--- Checks whether the Vector is within a box defined by the 2 other Vectors
---@param mins Vector First corner of the box
---@param maxs Vector Second corner of the box
---@return boolean # True if Vector is within the box, False otherwise
function Vector:within(mins, maxs)
	if self.x < math.min(mins.x, maxs.x) or self.x > math.max(mins.x, maxs.x) then return false end
	if self.y < math.min(mins.y, maxs.y) or self.y > math.max(mins.y, maxs.y) then return false end
	if self.z < math.min(mins.z, maxs.z) or self.z > math.max(mins.z, maxs.z) then return false end
	return true
end

-------------------------------------------

--- Returns cosine of the angle between both Vectors multiplied by their lengths
---@param v Vector
---@return number dot_product
function Vector:dot(v)
	return (self.x * v.x + self.y * v.y + self.z * v.z)
end

--- Returns a new perpendicular Vector based on self and the other Vector
---@param v Vector
---@return Vector new_vector
function Vector:cross(v)
	return wrap { x = self.y * v.z - self.z * v.y, y = self.z * v.x - self.x * v.z, z = self.x * v.y - self.y * v.x }
end

--- Returns Vector's length
---@return number length
function Vector:length()
	return math.sqrt(self.x^2 + self.y^2 + self.z^2)
end
Vector.magnitude = Vector.length

--- Returns Vector's length squared
---@return number length_squared
function Vector:length_sqr()
	return self.x^2 + self.y^2 + self.z^2
end
Vector.magnitude_sqr = Vector.length_sqr

--- Returns Pythagorean distance between self and the other Vector
---@param v Vector Other Vector
---@return number distance
function Vector:distance(v)
	return math.sqrt((v.x - self.x)^2 + (v.y - self.y)^2 + (v.z - self.z)^2)
end

--- Returns Pythagorean distance squared between self and the other Vector
---@param v Vector Other Vector
---@return number distance_squared
function Vector:distance_sqr(v)
	return (v.x - self.x)^2 + (v.y - self.y)^2 + (v.z - self.z)^2
end

--- Returns Manhattan distance to the other Vector
---@param v Vector Other Vector
---@return number manhattan_distance
function Vector:manhattan(v)
	return math.abs(self.x - v.x) + math.abs(self.y - v.y) + math.abs(self.z - v.z)
end

-------------------------------------------

--- Returns a new Vector with the same direction and length of 1
---@return Vector new_vector
function Vector:normalized()
	local len = math.sqrt(self.x^2 + self.y^2 + self.z^2)
	return wrap { x = self.x / len, y = self.y / len, z = self.z / len }
end

--- Returns a new Vector with floored X, Y, Z components
---@return Vector new_vector
function Vector:floored()
	return wrap { x = math.floor(self.x), y = math.floor(self.y), z = math.floor(self.z) }
end

--- Returns a new Vector with ceiled X, Y, Z components
---@return Vector new_vector
function Vector:ceiled()
	return wrap { x = math.ceil(self.x), y = math.ceil(self.y), z = math.ceil(self.z) }
end

--- Returns a new Vector with rounded X, Y, Z components
---@param decimals? integer Optional decimal place to round to, otherwise it'll round to nearest integer
---@return Vector new_vector
function Vector:rounded(decimals)
	return wrap { x = round(self.x, decimals), y = round(self.y, decimals), z = round(self.z, decimals) }
end

--- Returns a new Vector with X, Y, Z components constrained within `mins` and `maxs`
---@param mins Vector Minimum bounds
---@param maxs Vector Maximum bounds
---@return Vector new_vector
function Vector:clamped(mins, maxs)
	return wrap {
		x = clamp(self.x, math.min(mins.x, maxs.x), math.max(mins.x, maxs.x)),
		y = clamp(self.y, math.min(mins.y, maxs.y), math.max(mins.y, maxs.y)),
		z = clamp(self.z, math.min(mins.z, maxs.z), math.max(mins.z, maxs.z)),
	}
end

-------------------------------------------

--- **Self-modifying!**
--- Adds other Vector to self
---@param v Vector Vector to add
---@return Vector self
function Vector:add(v)
	self.x = self.x + v.x
	self.y = self.y + v.y
	self.z = self.z + v.z
	return self
end

--- **Self-modifying!**
--- Adds individual components to a self
---@param x number X component to add
---@param y number Y component to add
---@param z number Z component to add
---@return Vector self
function Vector:add_each(x, y, z)
	self.x = self.x + x
	self.y = self.y + y
	self.z = self.z + z
	return self
end

--- **Self-modifying!**
--- Subtracts other Vector from self
---@param v Vector Vector to subtract
---@return Vector self
function Vector:subtract(v)
	self.x = self.x - v.x
	self.y = self.y - v.y
	self.z = self.z - v.z
	return self
end
Vector.sub = Vector.subtract

--- **Self-modifying!**
--- Subtracts individual components to a self
---@param x number X component to subtract
---@param y number Y component to subtract
---@param z number Z component to subtract
---@return Vector self
function Vector:subtract_each(x, y, z)
	self.x = self.x - x
	self.y = self.y - y
	self.z = self.z - z
	return self
end
Vector.sub_each = Vector.subtract_each

--- **Self-modifying!**
--- Multiplies self by the other Vector
---@return Vector self
function Vector:multiply(v)
	self.x = self.x * v
	self.y = self.y * v
	self.z = self.z * v
	return self
end
Vector.mul = Vector.multiply

--- **Self-modifying!**
--- Multiplies self by individual components
---@param x number X component to multiply by
---@param y number Y component to multiply by
---@param z number Z component to multiply by
---@return Vector self
function Vector:multiply_each(x, y, z)
	self.x = self.x * x
	self.y = self.y * y
	self.z = self.z * z
	return self
end
Vector.mul_each = Vector.multiply_each

--- **Self-modifying!**
--- Divides self by the other Vector
---@return Vector self
function Vector:divide(v)
	self.x = self.x / v
	self.y = self.y / v
	self.z = self.z / v
	return self
end
Vector.div = Vector.divide

--- **Self-modifying!**
--- Divides self by individual components
---@param x number X component to divide by
---@param y number Y component to divide by
---@param z number Z component to divide by
---@return Vector self
function Vector:divide_each(x, y, z)
	self.x = self.x / x
	self.y = self.y / y
	self.z = self.z / z
	return self
end
Vector.div_each = Vector.divide_each

--- **Self-modifying!**
--- Copies X, Y, Z components from the given Vector or table containing x, y, z keys
---@return Vector self
function Vector:set(v)
	self.x = v.x
	self.y = v.y
	self.z = v.z
	return self
end

--- **Self-modifying!**
--- Set's X coordinate of self
---@param x number X coordinate
---@return Vector self
function Vector:set_x(x)
	self.x = x
	return self
end

--- **Self-modifying!**
--- Set's Y coordinate of self
---@param y number Y coordinate
---@return Vector self
function Vector:set_y(y)
	self.y = y
	return self
end

--- **Self-modifying!**
--- Set's Z coordinate of self
---@param z number Z coordinate
---@return Vector self
function Vector:set_z(z)
	self.z = z
	return self
end

-------------------------------------------

--- **Self-modifying!**
--- Normalizes Vector to the length of 1 without changing the direction
---@return Vector self
function Vector:normalize()
	local len = math.sqrt(self.x^2 + self.y^2 + self.z^2)
	self.x = self.x / len
	self.y = self.y / len
	self.z = self.z / len
	return self
end

--- **Self-modifying!**
--- Floors X, Y, Z components of the Vector
---@return Vector self
function Vector:floor()
	self.x = math.floor(self.x)
	self.y = math.floor(self.y)
	self.z = math.floor(self.z)
	return self
end

--- **Self-modifying!**
--- Ceils X, Y, Z components of the Vector
---@return Vector self
function Vector:ceil()
	self.x = math.ceil(self.x)
	self.y = math.ceil(self.y)
	self.z = math.ceil(self.z)
	return self
end

--- **Self-modifying!**
--- Rounds X, Y, Z components of the Vector
---@param decimals? integer Optional decimal place to round to, otherwise it'll round to nearest integer
---@return Vector self
function Vector:round(decimals)
	self.x = round(self.x, decimals)
	self.y = round(self.y, decimals)
	self.z = round(self.z, decimals)
	return self
end

--- **Self-modifying!**
--- Constrains X, Y, Z components of the Vector to `mins` and `maxs`
---@param mins Vector Minimum bounds
---@param maxs Vector Maximum bounds
---@return Vector self
function Vector:clamp(mins, maxs)
	self.x = clamp(self.x, math.min(mins.x, maxs.x), math.max(mins.x, maxs.x))
	self.y = clamp(self.y, math.min(mins.y, maxs.y), math.max(mins.y, maxs.y))
	self.z = clamp(self.z, math.min(mins.z, maxs.z), math.max(mins.z, maxs.z))
	return self
end

-------------------------------------------

---@class VectorLibrary
---@overload fun():Vector
local vector_library = {
	new    = new,
	wrap   = wrap,
	random = random,
}

return setmetatable(vector_library, {
	__call = function(_, ...)
		return new(...)
	end
})
